package us.csmckay.dl4j.translate;

import java.io.File;
import java.util.Collection;

import com.google.common.base.Joiner;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.MTNetwork;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.TMXParser;

/**
 * Encoder–decoder LSTM model trained to maximize the probability of a correct translation.
 */
public class NeuralTranslation {

  private static final Logger log = LoggerFactory.getLogger(NeuralTranslation.class);
  private static final String workPath = BaseUtils.getHomePath() + "/translate";
  private static final String remoteUrl = "http://opus.nlpl.eu/download.php?f=EMEA%2Fen-it.tmx.gz";

  public static void main(String[] args) {
    try {

      String dlFilePath = workPath + "/emea-en-it.gz";
      BaseUtils.download(remoteUrl, dlFilePath);
      String inFilePath = dlFilePath.replace(".gz", ".tmx");
      BaseUtils.extract("gz", dlFilePath, inFilePath);

      UIServer uiServer = UIServer.getInstance();
      StatsStorage statsStorage = new InMemoryStatsStorage();
      uiServer.attach(statsStorage);
      StatsListener statsListener = new StatsListener(statsStorage);

      MTNetwork mtNetwork = new MTNetwork(inFilePath, "en", "it", statsListener);

      TMXParser parser = new TMXParser(new File(inFilePath), "en", "it");
      Collection<TMXParser.ParallelSentence> parallelTestSentences = parser.parse();
      double accuracy = 0d;
      double i = 0d;
      long start = System.nanoTime();
      for (TMXParser.ParallelSentence parallelSentence : parallelTestSentences) {
        String source = parallelSentence.getSource();
        String decode = Joiner.on(' ').join(mtNetwork.output(source, 0));
        String target = parallelSentence.getTarget();
        if (target.trim().equalsIgnoreCase(decode.trim())) {
          accuracy++;
        }
        i++;
        if (i % 100 == 0) {
          log.info("s: " + source);
          log.info("t: " + target);
          log.info("d: " + decode);
          log.info("{} - {} trans/sec", accuracy / i, (i / (((System.nanoTime() - start) / 1e6) / 1e3)));
        }
      }
      accuracy /= parallelTestSentences.size();
      log.info("Accuracy: {}", accuracy);

      uiServer.stop();

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

}
