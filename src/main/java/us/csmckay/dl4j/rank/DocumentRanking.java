package us.csmckay.dl4j.rank;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.LMJelinekMercerSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.FieldValuesLabelAwareIterator;
import us.csmckay.dl4j.extension.ParagraphVectorsSimilarity;
import us.csmckay.dl4j.extension.WordEmbeddingsSimilarity;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.NetworkUtils;
import us.csmckay.dl4j.utility.WikipediaImport;

public class DocumentRanking {

  private static final Logger log = LoggerFactory.getLogger(DocumentRanking.class);
  private static final String workPath = BaseUtils.getHomePath() + "/rank";
  private static final String remoteUrl = "https://dumps.wikimedia.org/enwiki/20180701/enwiki-20180701-pages-articles1.xml-p10p30302.bz2";

  public static void main(String[] args) {
    try {

      String dlFilePath = workPath + "/enwiki.bz2";
      BaseUtils.download(remoteUrl, dlFilePath);
      String inFilePath = dlFilePath.replace(".bz2", "");
      BaseUtils.extract("bz2", dlFilePath, inFilePath);

      final String inputQuery = "bernhard riemann influence";
      final String fieldName = "title";
      File indexDir = new File(workPath + "/index");
      indexDir.mkdirs();
      Directory directory = FSDirectory.open(Paths.get(indexDir.getPath()));
      indexDocuments(inFilePath, directory, fieldName);

      // learn embeddings from index
      IndexReader reader = DirectoryReader.open(directory);
      FieldValuesLabelAwareIterator iterator = new FieldValuesLabelAwareIterator(reader, fieldName);

      int epochs = 20;
      int layerSize = 200;
      int windowSize = 3;

      Word2Vec w2vec = NetworkUtils.trainWord2Vec(iterator, layerSize, windowSize, epochs,
              workPath + "/w2v-model.zip");
      ParagraphVectors p2vec = NetworkUtils.trainParagraphVec(iterator, layerSize, epochs,
              workPath + "/p2v-model.zip");

      // search and ranking with different similarities
      Collection<Similarity> similarities = Arrays.asList(new ClassicSimilarity(), new BM25Similarity(2.5f, 0.2f),
              new LMJelinekMercerSimilarity(0.1f), new WordEmbeddingsSimilarity(w2vec, fieldName, true),
              new ParagraphVectorsSimilarity(p2vec, fieldName));

      log.info("[query] {}", inputQuery);
      for (Similarity similarity : similarities) {
        log.info("[similarity] {}", similarity.getClass().getName());
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);
        QueryParser parser = new QueryParser(fieldName, new WhitespaceAnalyzer());
        Query query = parser.parse(inputQuery);

        TopDocs hits = searcher.search(query, 10);
        for (int i = 0; i < hits.scoreDocs.length; i++) {
          ScoreDoc scoreDoc = hits.scoreDocs[i];
          Document doc = searcher.doc(scoreDoc.doc);
          log.info("[document{}] {} {}", scoreDoc.doc, doc.get(fieldName), scoreDoc.score);
          // log.debug(searcher.explain(query, scoreDoc.doc).toString());
        }
      }

      reader.close();
      directory.close();

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

  private static void indexDocuments(String wikiPath, Directory directory, String fieldName) throws Exception {
    if (directory.listAll().length == 0) {
      log.debug("Indexing documents...");
      IndexWriterConfig config = new IndexWriterConfig(new WhitespaceAnalyzer());
      IndexWriter writer = new IndexWriter(directory, config);

      FieldType ft = new FieldType(TextField.TYPE_STORED);
      ft.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
      ft.setTokenized(true);
      ft.setStored(true);
      ft.setStoreTermVectors(true);

      // adding our test documents
      Document doc1 = new Document();
      doc1.add(new Field(fieldName, "riemann bernhard - life and works of bernhard riemann", ft));
      Document doc2 = new Document();
      doc2.add(new Field(fieldName, "thomas bernhard biography - bio and influence in literature", ft));
      Document doc3 = new Document();
      doc3.add(new Field(fieldName, "riemann hypothesis - a deep dive into a mathematical mystery", ft));
      Document doc4 = new Document();
      doc4.add(new Field(fieldName,
              "bernhard bernhard bernhard bernhard bernhard " + "bernhard bernhard bernhard bernhard bernhard",
              ft));
      writer.addDocument(doc1);
      writer.addDocument(doc2);
      writer.addDocument(doc3);
      writer.addDocument(doc4);
      writer.commit();

      // adding more training data from Wikipedia
      new WikipediaImport(new File(wikiPath), "en", true).run(writer, ft);

      writer.commit();
      writer.close();
    } else {
      log.debug("Documents already indexed");
    }
  }

}
