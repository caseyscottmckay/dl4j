package us.csmckay.dl4j.search;

import java.io.File;
import java.util.Collection;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.FileSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.utility.BaseUtils;

public class W2VModelBuilder {

  private static final Logger log = LoggerFactory.getLogger(W2VModelBuilder.class);
  private static final String localPath = BaseUtils.getHomePath() + "/search";
  private static final String modelPath = localPath + "/w2v-model.zip";
  private static W2VModelBuilder instance;

  private W2VModelBuilder() {
  }

  public static W2VModelBuilder getInstance() {
    if (instance == null) {
      instance = new W2VModelBuilder();
    }
    return instance;
  }

  public static void main(String[] args) {
    Word2Vec net = W2VModelBuilder.getInstance().getModel();
    String[] words = new String[]{"guitar", "love", "life", "world", "woman"};
    for (String w : words) {
      // better results with wordsNearestSum instead of wordsNearest
      Collection<String> lst = net.wordsNearestSum(w, 5);
      log.info("words closest to '{}': {}", w, lst);
    }
    System.exit(0);
  }

  public Word2Vec getModel() {
    try {
      Word2Vec net = null;
      if (new File(modelPath).exists()) {
        log.debug("Loading model from {}", modelPath);
        net = WordVectorSerializer.readWord2VecModel(modelPath);
      } else {
        log.debug("Building a new model from raw data");
        net = W2VModelBuilder.buildModel();
      }
      return net;
    } catch (Exception e) {
      log.error("Unexpected error", e);
      return null;
    }
  }

  private static Word2Vec buildModel() {
    try {

      log.debug("Data load and vectorization...");
      SentenceIterator iter = new FileSentenceIterator(new File(localPath));
      iter.setPreProcessor(new SentencePreProcessor() {
        private static final long serialVersionUID = 1L;

        @Override
        public String preProcess(String sentence) {
          String[] fields = sentence.split(",");
          String lyrics = "";
          if (fields != null && fields.length > 4)
            lyrics = fields[4];
          return lyrics;
        }
      });

      TokenizerFactory t = new DefaultTokenizerFactory();
      t.setTokenPreProcessor(new CommonPreprocessor());

      log.debug("Network configuration and training...");
      Word2Vec net = new Word2Vec.Builder().seed(123).iterations(1).batchSize(1000).learningRate(0.06)
              .minLearningRate(0.03).minWordFrequency(15).layerSize(400).windowSize(5).iterate(iter)
              .tokenizerFactory(t).build();

      net.fit();
      WordVectorSerializer.writeWord2VecModel(net, modelPath);
      return net;

    } catch (Exception e) {
      log.error("Unexpected error", e);
      return null;
    }
  }

}
