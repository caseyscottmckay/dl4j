package us.csmckay.dl4j.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.synonym.SynonymGraphFilterFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.W2VSynFilterFactory;

public class Indexer {

  private static final Logger log = LoggerFactory.getLogger(Indexer.class);
  private String basePath;
  private IndexWriter writer;
  private boolean dynamicSynExp;

  public Indexer(String basePath, boolean dynamicSynExp) throws IOException {
    this.basePath = basePath;
    this.writer = buildIndexWriter(basePath);
    this.dynamicSynExp = dynamicSynExp;
  }

  public void close() throws CorruptIndexException, IOException {
    writer.close();
  }

  protected IndexWriter buildIndexWriter(String basePath) throws IOException {
    Directory directory = FSDirectory.open(Paths.get(basePath + "/index"));
    Analyzer indexTimeAnalyzer = buildIndexAnalyzer();
    Map<String, Analyzer> perFieldAnalyzers = new HashMap<>();
    perFieldAnalyzers.put("year", new KeywordAnalyzer());
    Analyzer analyzer = new PerFieldAnalyzerWrapper(indexTimeAnalyzer, perFieldAnalyzers);
    IndexWriterConfig config = new IndexWriterConfig(analyzer);
    IndexWriter writer = new IndexWriter(directory, config);
    return writer;
  }

  protected CustomAnalyzer buildIndexAnalyzer() throws IOException {
    // text analysis pipeline (tokenization and filtering) for all text fields
    // there is no stop words filter to enable exact song title search
    CustomAnalyzer.Builder builder = CustomAnalyzer.builder().withTokenizer(WhitespaceTokenizerFactory.class)
            .addTokenFilter(LowerCaseFilterFactory.class).addTokenFilter(SnowballPorterFilterFactory.class);
    if (dynamicSynExp) {
      W2VModelBuilder.getInstance().getModel();
      builder.addTokenFilter(W2VSynFilterFactory.class, "model", basePath + "/w2v-model.zip");
    } else {
      builder.addTokenFilter(SynonymGraphFilterFactory.class, "synonyms", "synonyms.txt");
    }
    return builder.build();
  }

  public void buildIndex() throws IOException {
    if (DirectoryReader.indexExists(writer.getDirectory())) {
      log.debug("Using previous index");
      return;
    }
    log.debug("Index building...");
    long start = System.nanoTime();
    for (File file : new File(basePath).listFiles()) {
      if (!file.isDirectory() && !file.isHidden() && file.exists() && file.canRead()
              && new CSVFileFilter().accept(file)) {
        indexFile(file);
      }
    }
    log.debug("{} documents indexed in {} ms", writer.numDocs(), (System.nanoTime() - start) / (long) 1e6);
  }

  /**
   * Data preparation and file indexing.
   */
  private void indexFile(File file) throws IOException {
    log.debug("Indexing {}", file.getCanonicalPath());
    BufferedReader br = new BufferedReader(new FileReader(file));
    String line;
    boolean firstLine = true;
    while ((line = br.readLine()) != null) {
      if (firstLine) {
        firstLine = false;
        continue;
      }
      String[] fields = line.split(",");
      if (fields != null && fields.length == 6) {
        String lyrics = normalizeText(fields[4]);
        if (lyrics.length() > 2) {
          // there will be an inverted index for each field
          Document doc = new Document();
          FieldType type = new FieldType(TextField.TYPE_STORED);
          doc.add(new Field("rank", fields[1], type));
          doc.add(new Field("song", normalizeText(fields[1]), type));
          doc.add(new Field("artist", normalizeText(fields[2]), type));
          doc.add(new Field("year", fields[3], type));
          doc.add(new Field("lyrics", lyrics, type));
          writer.addDocument(doc);
        }
      }
    }
    br.close();
    writer.commit();
  }

  /**
   * Remove all characters except [a-zA-Z0-9_] and space.
   */
  private String normalizeText(String value) {
    if (value == null || value.isEmpty())
      return value;
    return value.replaceAll("[^\\w\\s]", "").replaceAll("\\s{2,}", " ").trim();
  }

  public static class CSVFileFilter implements FileFilter {
    @Override
    public boolean accept(File pathname) {
      return pathname.getName().toLowerCase().endsWith(".csv");
    }
  }

}
