package us.csmckay.dl4j.search;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Searcher {

  private static final Logger log = LoggerFactory.getLogger(Searcher.class);
  private String basePath;
  private IndexReader reader;
  private IndexSearcher searcher;
  private QueryParser parser;

  public Searcher(String basePath) throws IOException {
    this.basePath = basePath;
    this.reader = buildIndexReader();
    this.searcher = new IndexSearcher(reader);
    Analyzer searchTimeAnalyzer = buildSearchAnalyzer();
    this.parser = new QueryParser("lyrics", searchTimeAnalyzer);
  }

  public void close() throws IOException {
    reader.close();
  }

  protected IndexReader buildIndexReader() throws IOException {
    Directory directory = FSDirectory.open(Paths.get(basePath + "/index"));
    IndexReader reader = DirectoryReader.open(directory);
    return reader;
  }

  protected CustomAnalyzer buildSearchAnalyzer() throws IOException {
    // text analysis pipeline (tokenization and filtering) for search queries
    // index and search time analyzers should be similar for better matches
    CustomAnalyzer.Builder builder = CustomAnalyzer.builder().withTokenizer(WhitespaceTokenizerFactory.class)
            .addTokenFilter(LowerCaseFilterFactory.class).addTokenFilter(SnowballPorterFilterFactory.class);
    return builder.build();
  }

  public TopDocs search(String input) throws IOException, ParseException {
    return search(input, 10);
  }

  public TopDocs search(String input, int maxResults) throws IOException, ParseException {
    long start = System.nanoTime();
    Query query = parser.parse(input);
    TopDocs hits = searcher.search(query, maxResults);
    log.info("{} documents found in {} ms", hits.totalHits, ((System.nanoTime() - start) / (long) 1e6));
    return hits;
  }

  public Document getDocument(ScoreDoc scoreDoc) throws CorruptIndexException, IOException {
    return searcher.doc(scoreDoc.doc);
  }

}
