package us.csmckay.dl4j.search;

import java.util.Scanner;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.utility.BaseUtils;

public class SearchEngine {

  private static final Logger log = LoggerFactory.getLogger(SearchEngine.class);
  private static final String remoteUrl = "https://github.com/walkerkq/musiclyrics/blob/master/billboard_lyrics_1964-2015.csv?raw=true";
  private static final String workPath = BaseUtils.getHomePath() + "/search";

  @SuppressWarnings("resource")
  public static void main(String[] args) {
    SearchEngine searchEngine = new SearchEngine();
    while (true) {
      log.info("\n\nNew search (q to quit):");
      String input = new Scanner(System.in).nextLine();
      if (input.equalsIgnoreCase("q")) {
        System.exit(0);
      }
      searchEngine.search(input);
    }
  }

  public SearchEngine() {
    try {
      BaseUtils.download(remoteUrl, workPath + "/billboard.csv");
      Indexer indexer = new Indexer(workPath, false);
      indexer.buildIndex();
      indexer.close();
    } catch (Exception e) {
      log.error("Init error", e);
    }
  }

  private void search(String inputQuery) {
    try {
      Searcher searcher = new Searcher(workPath);
      TopDocs hits = searcher.search(inputQuery);
      for (ScoreDoc scoreDoc : hits.scoreDocs) {
        Document doc = searcher.getDocument(scoreDoc);
        log.info("'{}' by '{}'", doc.get("song"), doc.get("artist"));
      }
      searcher.close();
    } catch (Exception e) {
      log.error("Search error", e);
    }
  }

}
