package us.csmckay.dl4j.recommend;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;

import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.FieldValuesLabelAwareIterator;
import us.csmckay.dl4j.extension.ParagraphVectorsSimilarity;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.NetworkUtils;
import us.csmckay.dl4j.utility.WikipediaImport;

/**
 * Related content suggestions. You'll need more than 8GB of RAM to run this one.
 */
public class RelatedContent {

  private static final Logger log = LoggerFactory.getLogger(RelatedContent.class);
  private static final String workPath = BaseUtils.getHomePath() + "/chapter6";
  private static final String wikiPath = "c:/Users/fvaleri/.dl4search/chapter5/enwiki-20180320-pages-articles1.xml-p10p30302";

  public static void main(String[] args) {
    try {

      final String inputQuery = "Ledgewood Circle";
      final String fieldName = "text";
      File indexDir = new File(workPath + "/index");
      indexDir.mkdirs();
      Directory directory = FSDirectory.open(Paths.get(indexDir.getPath()));
      indexDocuments(directory, fieldName);

      // learn embeddings from index
      IndexReader reader = DirectoryReader.open(directory);
      FieldValuesLabelAwareIterator iterator = new FieldValuesLabelAwareIterator(reader, fieldName);
      ParagraphVectors net = NetworkUtils.trainParagraphVec(iterator, 200, 3, workPath + "/p2v-model.zip");

      // find nearest labels
      log.info("[query] {}", inputQuery);
      IndexSearcher searcher = new IndexSearcher(reader);
      searcher.setSimilarity(new ParagraphVectorsSimilarity(net, fieldName));
      QueryParser parser = new QueryParser(fieldName, new WhitespaceAnalyzer());
      Query query = parser.parse(inputQuery);

      int topN = 10;
      TopDocs hits = searcher.search(query, topN);
      for (int i = 0; i < hits.scoreDocs.length; i++) {
        ScoreDoc scoreDoc = hits.scoreDocs[i];
        Document doc = searcher.doc(scoreDoc.doc);
        log.info("[document{}] {} {}", scoreDoc.doc, doc.get("title"), scoreDoc.score);

        String label = "doc_" + scoreDoc.doc;
        INDArray labelVector = net.getLookupTable().vector(label);
        Collection<String> docIds = net.nearestLabels(labelVector, topN);
        for (String docId : docIds) {
          int id = Integer.parseInt(docId.substring(4));
          log.info("[related] {}", reader.document(id).get("title"));
        }
      }

      reader.close();
      directory.close();

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

  private static void indexDocuments(Directory directory, final String fieldName) throws IOException, Exception {
    if (directory.listAll().length == 0) {
      log.debug("Indexing documents...");
      IndexWriterConfig config = new IndexWriterConfig(new WhitespaceAnalyzer());
      IndexWriter writer = new IndexWriter(directory, config);
      FieldType ft = new FieldType(TextField.TYPE_STORED);
      ft.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
      ft.setTokenized(true);
      ft.setStored(true);
      ft.setStoreTermVectors(true);
      new WikipediaImport(new File(wikiPath), "en", true).run(writer, ft);
      writer.commit();
      writer.close();
    } else {
      log.debug("Documents already indexed");
    }
  }

}
