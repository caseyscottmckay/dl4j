package us.csmckay.dl4j.generate;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.CharacterIterator;
import us.csmckay.dl4j.extension.LSTMQueryParser;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.NetworkUtils;


public class AutoQueryExpansion {

  private static final Logger log = LoggerFactory.getLogger(AutoQueryExpansion.class);
  private static final String resourcePath = BaseUtils.getResourcePath("queries.txt");
  private static final String workPath = BaseUtils.getHomePath() + "/generate";

  public static void main(String[] args) {
    try {

      // TODO search for a better dataset
      File dataFile = new File(workPath + "/queries.txt");
      FileUtils.copyFile(new File(resourcePath), dataFile);

      NetworkUtils.NetParams params = new NetworkUtils.NetParams();
      params.setExampleLength(40);
      params.setTbpttLength(50);
      params.setMiniBatchSize(30);
      params.setNumHiddenLayers(2);
      params.setHiddenLayerSize(150);
      params.setNumEpochs(1);
      params.setLearningRate(0.08);
      params.setWeightInit(WeightInit.XAVIER);
      params.setUpdater(Updater.RMSPROP);
      params.setActivation(Activation.TANH);
      params.setInitSequence("latest trends\n");
      params.setCharsToSample(40);
      params.setSamplesToGen(3);
      params.setEosChar('\n');
      params.setGenEveryNMiniBatch(30);

      CharacterIterator iter = new CharacterIterator(dataFile.getPath(), Charset.forName("UTF-8"),
              params.getMiniBatchSize(), params.getExampleLength());
      params.setCharIter(iter);
      params.setInputLayerSize(iter.inputColumns());
      params.setOutputLayerSize(iter.inputColumns());

      MultiLayerNetwork net = NetworkUtils.trainLSTM(params);
      ModelSerializer.writeModel(net, workPath + "/aqe-model.zip", true);

      LSTMQueryParser parser = new LSTMQueryParser("text", new EnglishAnalyzer(null), net, iter);
      String[] inputQueries = new String[]{"latest trends", "covfefe", "concerts", "music events"};
      for (String inputQuery : inputQueries) {
        log.info(parser.parse(inputQuery).toString());
      }

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

}