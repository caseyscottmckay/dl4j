package us.csmckay.dl4j.generate;

import java.nio.charset.Charset;

import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.CharacterIterator;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.NetworkUtils;

public class ShakespeareGenerator {

  private static final Logger log = LoggerFactory.getLogger(ShakespeareGenerator.class);
  private static final String remoteUrl = "https://s3.amazonaws.com/dl4j-distribution/pg100.txt";
  private static final String workPath = BaseUtils.getHomePath() + "/generate";

  public static void main(String[] args) {
    try {

      String filePath = workPath + "/shakespeare.txt";
      BaseUtils.download(remoteUrl, filePath);

      NetworkUtils.NetParams params = new NetworkUtils.NetParams();
      params.setExampleLength(1000);
      params.setTbpttLength(50);
      params.setMiniBatchSize(32);
      params.setNumHiddenLayers(1);
      params.setHiddenLayerSize(200);
      params.setNumEpochs(1);
      params.setLearningRate(0.1);
      params.setWeightInit(WeightInit.XAVIER);
      params.setUpdater(Updater.RMSPROP);
      params.setActivation(Activation.TANH);
      params.setCharsToSample(300);
      params.setSamplesToGen(4);
      params.setGenEveryNMiniBatch(10);

      CharacterIterator iter = new CharacterIterator(filePath, Charset.forName("UTF-8"),
              params.getMiniBatchSize(), params.getExampleLength());
      params.setCharIter(iter);
      params.setInputLayerSize(iter.inputColumns());
      params.setOutputLayerSize(iter.inputColumns());

      MultiLayerNetwork net = NetworkUtils.trainLSTM(params);
      ModelSerializer.writeModel(net, workPath + "/sg-model.zip", true);

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

}
