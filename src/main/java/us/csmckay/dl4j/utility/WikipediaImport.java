package us.csmckay.dl4j.utility;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WikipediaImport {

  private static final Logger log = LoggerFactory.getLogger(WikipediaImport.class);
  private static final Pattern pattern = Pattern.compile("\\[Category\\:(\\w+([\\|\\s\\']\\w*)*)\\]");
  private static final String CATEGORY = "category";
  private static final String TEXT = "text";
  private static final String TITLE = "title";
  private static final String LANG = "lang";
  private final File dumpFile;
  private final String languageCode;
  private final boolean doReport;

  public WikipediaImport(File dumpFile, String languageCode, boolean doReport) {
    this.dumpFile = dumpFile;
    this.languageCode = languageCode;
    this.doReport = doReport;
  }

  public void run(IndexWriter indexWriter, FieldType ft) throws Exception {
    long start = System.nanoTime();
    int count = 0;
    if (doReport) {
      log.debug("Importing {}", dumpFile.getPath());
    }

    String title = null;
    String text = null;
    Set<String> cats = new HashSet<>();
    XMLInputFactory factory = XMLInputFactory.newInstance();
    StreamSource source;
    source = new StreamSource(dumpFile);
    XMLStreamReader reader = factory.createXMLStreamReader(source);
    while (reader.hasNext()) {
      switch (reader.next()) {
        case XMLStreamConstants.START_ELEMENT:
          if ("title".equals(reader.getLocalName())) {
            title = reader.getElementText();
          } else if ("text".equals(reader.getLocalName())) {
            text = reader.getElementText();
            Matcher matcher = pattern.matcher(text);
            int pos = 0;
            while (matcher.find(pos)) {
              String group = matcher.group(1);
              String catName = group.replaceAll("\\|\\s", "").replaceAll("\\|\\*", "");
              Collections.addAll(cats, catName.split("\\|"));
              pos = matcher.end();
            }
          }
          break;
        case XMLStreamConstants.END_ELEMENT:
          if ("page".equals(reader.getLocalName())) {
            Document page = new Document();
            page.add(new Field(TITLE, title, ft));
            page.add(new Field(TEXT, text, ft));
            for (String cat : cats) {
              page.add(new Field(CATEGORY, cat, ft));
            }
            page.add(new StringField(LANG, languageCode, Field.Store.YES));
            indexWriter.addDocument(page);
            count++;
            if (count % 10000 == 0) {
              batchDone(indexWriter, start, count);
            }
            cats.clear();
          }
          break;
      }
    }

    indexWriter.commit();

    if (doReport) {
      double millis = (System.nanoTime() - start) / (long) 1e6;
      log.debug("Imported {} pages in {} seconds ({} ms/page)", count, millis / 1000, millis / count);
    }
  }

  private void batchDone(IndexWriter indexWriter, long start, int count) throws IOException {
    indexWriter.commit();
    if (doReport) {
      double millis = (System.nanoTime() - start) / (long) 1e6;
      log.debug("Added {} pages in {} seconds ({} ms/page)", count, (millis / 1000), millis / count);
    }
  }

}