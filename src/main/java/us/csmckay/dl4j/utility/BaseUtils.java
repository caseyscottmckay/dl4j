package us.csmckay.dl4j.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class BaseUtils {

  private static final Logger log = LoggerFactory.getLogger(BaseUtils.class);

  public static String getHomePath() {
    final String path = System.getProperty("user.home") + "/.dl4search";
    new File(path).mkdirs();
    return path;
  }

  public static String getResourcePath(String res) {
    URL url = BaseUtils.class.getClassLoader().getResource(res);
    return url.getPath();
  }

  /**
   * Download a remote file.
   *
   * @param remoteUrl URL of the remote file.
   * @param localPath Where to download the file.
   * @return True if and only if the file has been downloaded.
   * @throws IOException IO error.
   */
  public static boolean download(String remoteUrl, String localPath) throws IOException {
    boolean res = false;
    if (remoteUrl == null || localPath == null) {
      return res;
    }
    File file = new File(localPath);
    if (!file.exists()) {
      log.debug("Downloading from {}", remoteUrl);
      file.getParentFile().mkdirs();
      HttpClientBuilder builder = HttpClientBuilder.create();
      CloseableHttpClient client = builder.build();
      try (CloseableHttpResponse response = client.execute(new HttpGet(remoteUrl))) {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          try (FileOutputStream outstream = new FileOutputStream(file)) {
            entity.writeTo(outstream);
            outstream.flush();
            outstream.close();
          }
        }
      }
      res = true;
      log.debug("Download complete");
    }
    if (!file.exists()) {
      throw new IOException("File doesn't exist: " + localPath);
    }
    return res;
  }

  /**
   * Extract a compressed file.
   *
   * @param compType   Compression type (i.e. tar.gz, gz, bz2).
   * @param inputPath  Input file path.
   * @param outputPath Output file path.
   * @throws IOException IO error.
   */
  public static void extract(String compType, String inputPath, String outputPath) throws IOException {
    if (compType == null || inputPath == null || outputPath == null) {
      return;
    }
    if (!new File(outputPath).exists()) {
      log.debug("Extracting to {}", outputPath);
      switch (compType.toLowerCase()) {
        case "tar.gz":
          extractTarGz(inputPath, outputPath);
          break;
        case "gz":
          extractGz(inputPath, outputPath);
          break;
        case "bz2":
          extractBz2(inputPath, outputPath);
          break;
        default:
          throw new RuntimeException("Unknown archive type: " + compType);
      }

      log.debug("Extraction complete");
    }
  }

  private static void extractTarGz(String inputPath, String outputPath) throws IOException, FileNotFoundException {
    final int bufferSize = 4096;
    try (TarArchiveInputStream in = new TarArchiveInputStream(
            new GzipCompressorInputStream(new BufferedInputStream(new FileInputStream(inputPath))))) {
      TarArchiveEntry entry;
      while ((entry = (TarArchiveEntry) in.getNextEntry()) != null) {
        if (entry.isDirectory()) {
          new File(outputPath + entry.getName()).mkdirs();
        } else {
          BufferedOutputStream out = new BufferedOutputStream(
                  new FileOutputStream(outputPath + entry.getName()), bufferSize);
          int n;
          byte data[] = new byte[bufferSize];
          while ((n = in.read(data, 0, bufferSize)) != -1) {
            out.write(data, 0, n);
          }
          out.close();
          in.close();
        }
      }
    }
  }

  private static void extractGz(String inputPath, String outputPath) throws IOException, FileNotFoundException {
    final int bufferSize = 4096;
    byte[] data = new byte[bufferSize];
    GzipCompressorInputStream in = new GzipCompressorInputStream(new FileInputStream(inputPath));
    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outputPath), bufferSize);
    int len;
    while ((len = in.read(data)) >= 0) {
      out.write(data, 0, len);
    }
    out.close();
    in.close();
  }

  private static void extractBz2(String inputPath, String outputPath) throws IOException, FileNotFoundException {
    final int bufferSize = 4096;
    BZip2CompressorInputStream in = new BZip2CompressorInputStream(new FileInputStream(inputPath));
    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outputPath), bufferSize);
    int n;
    byte[] data = new byte[bufferSize];
    while ((n = in.read(data, 0, bufferSize)) != -1) {
      out.write(data, 0, n);
    }
    out.close();
    in.close();
  }

  /**
   * Build a document vector by mean of all its word vectors.
   *
   * @param vec Trained Word2Vec instance.
   * @param str Document text.
   * @return Document vector.
   */
  public static INDArray toDenseMeanVector(Word2Vec vec, String str) throws IOException {
    String[] words = str.split(" ");
    return vec.getWordVectorsMean(Arrays.asList(words));
  }

  public static double tfIdf(double n, double termFreq, double docFreq) {
    // weight(term) = (1+log(tf(term)))*log(N/df(term))
    return 1 + Math.log(termFreq) * Math.log(n / docFreq);
  }

  /**
   * Build a document vector by mean of all its word vectors.
   *
   * @param vec      Trained Word2Vec instance.
   * @param docTerms Document terms.
   * @param numDocs  Total number of documents.
   * @param tfidf    Use TF-IDF smoothing.
   * @return Document vector.
   * @throws IOException IO error.
   */
  public static INDArray toDenseMeanVector(Word2Vec vec, Terms docTerms, double numDocs, boolean tfidf)
          throws IOException {
    INDArray docVector = Nd4j.zeros(vec.getLayerSize());
    if (docTerms != null) {
      TermsEnum iter = docTerms.iterator();
      BytesRef term;
      while ((term = iter.next()) != null) {
        INDArray wordVector = vec.getLookupTable().vector(term.utf8ToString());
        if (wordVector != null) {
          double smoothFactor;
          if (tfidf) {
            // docVector(wordA wordB) = wordVector(wordA)/tfIdf(wordA) +
            // wordVector(wordB)/tfIdf(wordB)
            smoothFactor = tfIdf(numDocs, iter.totalTermFreq(), iter.docFreq());
          } else {
            smoothFactor = docTerms.size();
          }
          docVector.addi(wordVector.div(smoothFactor));
        }
      }
    }
    return docVector;
  }

}
