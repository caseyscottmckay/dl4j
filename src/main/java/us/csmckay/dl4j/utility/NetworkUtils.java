package us.csmckay.dl4j.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.CharacterIterator;
import us.csmckay.dl4j.extension.FieldValuesLabelAwareIterator;

public class NetworkUtils {

  private static final Logger log = LoggerFactory.getLogger(NetworkUtils.class);
  private static UIServer uiServer;

  /**
   * Build and train a W2V net.
   *
   * @param iter       FieldValuesLabelAwareIterator.
   * @param layerSize  Layer size.
   * @param windowSize Window size.
   * @param epochs     Number of epochs.
   * @param modelPath  Model home.
   * @return Trained net.
   */
  public static Word2Vec trainWord2Vec(FieldValuesLabelAwareIterator iter, int layerSize, int windowSize, int epochs,
                                       String modelPath) {
    Word2Vec net = null;
    if (!new File(modelPath).exists()) {
      log.debug("Learning Word2Vec...");
      net = new Word2Vec.Builder().layerSize(layerSize).windowSize(windowSize)
              .tokenizerFactory(new DefaultTokenizerFactory()).iterate(iter).epochs(epochs).seed(12345).build();
      net.fit();
      WordVectorSerializer.writeWord2VecModel(net, modelPath);
    } else {
      log.debug("Loading Word2Vec model...");
      net = WordVectorSerializer.readWord2VecModel(modelPath);
    }
    return net;
  }

  /**
   * Build and train a P2V net.
   *
   * @param iter      FieldValuesLabelAwareIterator.
   * @param layerSize Layer size.
   * @param epochs    Number of epochs.
   * @param modelPath Model home.
   * @return Trained net.
   */
  public static ParagraphVectors trainParagraphVec(FieldValuesLabelAwareIterator iter, int layerSize, int epochs,
                                                   String modelPath) throws IOException {
    ParagraphVectors net = null;
    if (!new File(modelPath).exists()) {
      log.debug("Learning ParagraphVectors...");
      net = new ParagraphVectors.Builder().layerSize(layerSize).tokenizerFactory(new DefaultTokenizerFactory())
              .iterate(iter).epochs(epochs).seed(12345).build();
      net.fit();
      WordVectorSerializer.writeParagraphVectors(net, modelPath);
    } else {
      log.debug("Loading ParagraphVectors model...");
      net = WordVectorSerializer.readParagraphVectors(modelPath);
      net.setTokenizerFactory(new DefaultTokenizerFactory());
    }
    return net;
  }

  /**
   * Build and train a LSTM net.
   *
   * @param np Network parameters.
   * @return Trained net.
   */
  public static MultiLayerNetwork trainLSTM(NetParams np) {
    log.debug("Building LSTM network...");
    Nd4j.getMemoryManager().setAutoGcWindow(5000);

    NeuralNetConfiguration.ListBuilder builder = new NeuralNetConfiguration.Builder()
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1).seed(12345)
            .learningRate(np.getLearningRate()).regularization(true).l2(0.001)
            .inferenceWorkspaceMode(WorkspaceMode.SINGLE).trainingWorkspaceMode(WorkspaceMode.SINGLE)
            .weightInit(np.getWeightInit()).updater(np.getUpdater()).list()
            .layer(0, new LSTM.Builder().nIn(np.getInputLayerSize()).nOut(np.getHiddenLayerSize())
                    .activation(np.getActivation()).build());
    for (int i = 1; i < np.getNumHiddenLayers(); i++) {
      builder = builder.layer(i, new LSTM.Builder().nIn(np.getHiddenLayerSize()).nOut(np.getHiddenLayerSize())
              .activation(np.getActivation()).build());
    }
    builder.layer(np.getNumHiddenLayers(),
            new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT).activation(Activation.SOFTMAX)
                    .nIn(np.getHiddenLayerSize()).nOut(np.getOutputLayerSize()).build())
            .backpropType(BackpropType.TruncatedBPTT).tBPTTForwardLength(np.getTbpttLength())
            .tBPTTBackwardLength(np.getTbpttLength()).build();

    MultiLayerConfiguration conf = builder.build();
    MultiLayerNetwork net = new MultiLayerNetwork(conf);
    net.init();
    uiServer = UIServer.getInstance();
    StatsStorage statsStorage = new InMemoryStatsStorage();
    uiServer.attach(statsStorage);
    net.setListeners(new ScoreIterationListener(10), new StatsListener(statsStorage));
    log.debug("Network parameters: {}", net.numParams());

    log.debug("Training LSTM network...");
    int miniBatchNumber = 0;
    long start = System.nanoTime();
    for (int i = 0; i < np.getNumEpochs(); i++) {
      CharacterIterator iter = np.getCharIter();
      while (iter.hasNext()) {
        DataSet ds = iter.next();
        net.fit(ds);
        if (++miniBatchNumber % np.getGenEveryNMiniBatch() == 0) {
          log.debug("Completed {} mini-batches of size {}x{} chars", miniBatchNumber, np.getMiniBatchSize(),
                  np.getExampleLength());
          List<Sample> genSamples = sampleFromNetwork(iter, net, np);
          for (int j = 0; j < genSamples.size(); j++)
            log.debug("[SAMPLE{}]\n\n[{}]\n", j, genSamples.get(j).getValue());
        }
      }
      log.debug("Completed epoch {}", i);
      iter.reset();
    }
    log.debug("Training completed in {} ms", (System.nanoTime() - start) / (long) 1e6);
    stopListeners(net);
    return net;
  }

  private static void stopListeners(MultiLayerNetwork net) {
    if (net.getListeners().size() > 0) {
      uiServer.stop();
      net.setListeners(Collections.emptySet());
      System.gc();
    }
  }

  /**
   * Create one input hot-vector for each char in the init sequence.
   *
   * @param iter       {@link CharacterIterator}
   * @param initSeq    Initialization sequence.
   * @param numSamples Number of samples to generate.
   * @return Initialization array.
   */
  public static INDArray encodeCharacters(CharacterIterator iter, String initSeq, int numSamples) {
    INDArray initArray = Nd4j.zeros(numSamples, iter.inputColumns(), initSeq.length());
    char[] init = initSeq.toCharArray();
    for (int i = 0; i < init.length; i++) {
      int idx = iter.getCharacterIndex(init[i]);
      for (int j = 0; j < numSamples; j++)
        initArray.putScalar(new int[]{j, idx, i}, 1.0f);
    }
    return initArray;
  }

  /**
   * Generate a new sample from the network, given the init sequence. Init
   * sequence can be used to 'prime' the RNN with a sequence you want to continue.
   *
   * @param iter CharacterIterator used for going from indexes back to characters.
   * @param net  Trained LSTM net with a softmax output layer.
   * @param np   Network parameters.
   * @return Generated samples list.
   */
  public static List<Sample> sampleFromNetwork(CharacterIterator iter, MultiLayerNetwork net, NetParams np) {
    if (np.getInitSequence() == null || np.getInitSequence().isEmpty())
      np.setInitSequence(String.valueOf(iter.getRandomCharacter()));
    INDArray initArray = encodeCharacters(iter, np.getInitSequence(), np.getSamplesToGen());

    StringBuilder[] sb = new StringBuilder[np.getSamplesToGen()];
    for (int i = 0; i < np.getSamplesToGen(); i++)
      sb[i] = new StringBuilder(np.getInitSequence());

    // get the last time step output
    net.rnnClearPreviousState();
    INDArray output = net.rnnTimeStep(initArray);
    output = output.tensorAlongDimension(output.size(2) - 1, 1, 0);

    // sample from network (and feed samples back into input) one character at a
    // time
    List<Double> charsCondProb = new ArrayList<>(np.getSamplesToGen());
    for (int i = 0; i < np.getCharsToSample(); i++) {
      // set up next input (single time step) by sampling from previous output
      INDArray nextInput = Nd4j.zeros(np.getSamplesToGen(), iter.inputColumns());
      // sample from the out dist for each sample we want to generate and add it to
      // the new input
      for (int s = 0; s < np.getSamplesToGen(); s++) {
        double[] outputProbDist = new double[iter.totalOutcomes()];
        for (int j = 0; j < outputProbDist.length; j++)
          outputProbDist[j] = output.getDouble(s, j);
        Sample charSample = sampleFromDistribution(outputProbDist);
        charsCondProb.add(charSample.getProb());
        nextInput.putScalar(new int[]{s, charSample.getIndex()}, 1.0f); // prepare next time step input
        char c = iter.getCharacter(charSample.getIndex());
        if (np.getEosChar() != null && np.getEosChar() == c)
          break;
        sb[s].append(c);
      }
      output = net.rnnTimeStep(nextInput); // do one time step of forward pass
    }

    // recover generated samples
    List<Sample> samples = new ArrayList<>();
    for (int i = 0; i < np.getSamplesToGen(); i++) {
      String value = sb[i].toString();
      // TODO we should compute generated samples prob. from chars cond. prob.
      Sample netSample = new Sample(value, charsCondProb.get(i));
      samples.add(netSample);
    }

    return samples;
  }

  /**
   * Pick a random sample from a given probability distribution.
   *
   * @param dist Probability distribution over discrete classes (must add up to
   *             1).
   * @return The char sample containing its probability char index.
   */
  public static Sample sampleFromDistribution(double[] dist) {
    Random rng = new Random();
    double d = 0.0;
    double sum = 0.0;
    for (int t = 0; t < 10; t++) {
      d = rng.nextDouble();
      sum = 0.0;
      for (int i = 0; i < dist.length; i++) {
        sum += dist[i];
        if (d <= sum) {
          return new Sample(d, i);
        }
      }
      // if we haven't found the right index yet, maybe the sum is
      // slightly lower than 1 due to rounding error, so try again
    }
    throw new IllegalArgumentException("Invalid distribution? d=" + d + ", sum=" + sum);
  }

  public static class NetParams {
    private int inputLayerSize;
    private int numHiddenLayers;
    private int hiddenLayerSize;
    private int outputLayerSize;
    private double learningRate;
    private int tbpttLength;
    private int miniBatchSize;
    private WeightInit weightInit;
    private Updater updater;
    private Activation activation;
    private int numEpochs;

    private CharacterIterator charIter;
    private int genEveryNMiniBatch;
    private String initSequence;
    private int exampleLength;
    private int charsToSample;
    private int samplesToGen;
    private Character eosChar;

    public int getInputLayerSize() {
      return inputLayerSize;
    }

    public void setInputLayerSize(int inputLayerSize) {
      this.inputLayerSize = inputLayerSize;
    }

    public int getNumHiddenLayers() {
      return numHiddenLayers;
    }

    public void setNumHiddenLayers(int numHiddenLayers) {
      this.numHiddenLayers = numHiddenLayers;
    }

    public int getHiddenLayerSize() {
      return hiddenLayerSize;
    }

    public void setHiddenLayerSize(int hiddenLayerSize) {
      this.hiddenLayerSize = hiddenLayerSize;
    }

    public int getOutputLayerSize() {
      return outputLayerSize;
    }

    public void setOutputLayerSize(int outputLayerSize) {
      this.outputLayerSize = outputLayerSize;
    }

    public double getLearningRate() {
      return learningRate;
    }

    public void setLearningRate(double learningRate) {
      this.learningRate = learningRate;
    }

    public int getTbpttLength() {
      return tbpttLength;
    }

    public void setTbpttLength(int tbpttLength) {
      this.tbpttLength = tbpttLength;
    }

    public int getMiniBatchSize() {
      return miniBatchSize;
    }

    public void setMiniBatchSize(int miniBatchSize) {
      this.miniBatchSize = miniBatchSize;
    }

    public WeightInit getWeightInit() {
      return weightInit;
    }

    public void setWeightInit(WeightInit weightInit) {
      this.weightInit = weightInit;
    }

    public Updater getUpdater() {
      return updater;
    }

    public void setUpdater(Updater updater) {
      this.updater = updater;
    }

    public Activation getActivation() {
      return activation;
    }

    public void setActivation(Activation activation) {
      this.activation = activation;
    }

    public int getNumEpochs() {
      return numEpochs;
    }

    public void setNumEpochs(int numEpochs) {
      this.numEpochs = numEpochs;
    }

    public CharacterIterator getCharIter() {
      return charIter;
    }

    public void setCharIter(CharacterIterator charIter) {
      this.charIter = charIter;
    }

    public int getGenEveryNMiniBatch() {
      return genEveryNMiniBatch;
    }

    public void setGenEveryNMiniBatch(int genEveryNMiniBatch) {
      this.genEveryNMiniBatch = genEveryNMiniBatch;
    }

    public String getInitSequence() {
      return initSequence;
    }

    public void setInitSequence(String initSequence) {
      this.initSequence = initSequence;
    }

    public int getExampleLength() {
      return exampleLength;
    }

    public void setExampleLength(int exampleLength) {
      this.exampleLength = exampleLength;
    }

    public int getCharsToSample() {
      return charsToSample;
    }

    public void setCharsToSample(int charsToSample) {
      this.charsToSample = charsToSample;
    }

    public int getSamplesToGen() {
      return samplesToGen;
    }

    public void setSamplesToGen(int samplesToGen) {
      this.samplesToGen = samplesToGen;
    }

    public Character getEosChar() {
      return eosChar;
    }

    public void setEosChar(Character eosChar) {
      this.eosChar = eosChar;
    }

    @Override
    public String toString() {
      return "NetParams [inputLayerSize=" + inputLayerSize + ", numHiddenLayers=" + numHiddenLayers
              + ", hiddenLayerSize=" + hiddenLayerSize + ", outputLayerSize=" + outputLayerSize
              + ", learningRate=" + learningRate + ", tbpttLength=" + tbpttLength + ", miniBatchSize="
              + miniBatchSize + ", weightInit=" + weightInit + ", updater=" + updater + ", activation="
              + activation + ", numEpochs=" + numEpochs + ", charIter=" + charIter + ", genEveryNMiniBatch="
              + genEveryNMiniBatch + ", initSequence=" + initSequence + ", exampleLength=" + exampleLength
              + ", charsToSample=" + charsToSample + ", samplesToGen=" + samplesToGen + ", eosChar=" + eosChar
              + "]";
    }
  }

  public static class Sample {
    private String value;
    private double prob;
    private int index;

    public Sample(String value, double prob) {
      this.value = value;
      this.prob = prob;
    }

    public Sample(double prob, int index) {
      this.prob = prob;
      this.index = index;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }

    public double getProb() {
      return prob;
    }

    public void setProb(double prob) {
      this.prob = prob;
    }

    public int getIndex() {
      return index;
    }

    public void setIndex(int index) {
      this.index = index;
    }

    @Override
    public String toString() {
      return "Sample [value=" + value + ", prob=" + prob + ", index=" + index + "]";
    }
  }

}
