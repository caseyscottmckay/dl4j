package us.csmckay.dl4j.suggest;

import java.io.File;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.search.suggest.Lookup;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.extension.CharacterIterator;
import us.csmckay.dl4j.extension.LSTMLookup;
import us.csmckay.dl4j.utility.BaseUtils;
import us.csmckay.dl4j.utility.NetworkUtils;

public class LSTMQuerySuggestions {

  private static final Logger log = LoggerFactory.getLogger(LSTMQuerySuggestions.class);
  private static final String resourcePath = BaseUtils.getResourcePath("queries.txt");
  private static final String workPath = BaseUtils.getHomePath() + "/suggest";

  public static void main(String[] args) {
    try {

      // TODO search for a better dataset
      String filePath = workPath + "/queries.txt";
      FileUtils.copyFile(new File(resourcePath), new File(filePath));

      NetworkUtils.NetParams params = new NetworkUtils.NetParams();
      params.setExampleLength(1000);
      params.setTbpttLength(125);
      params.setMiniBatchSize(10);
      params.setNumHiddenLayers(3);
      params.setHiddenLayerSize(500);
      params.setNumEpochs(1);
      params.setLearningRate(0.0005);
      params.setWeightInit(WeightInit.XAVIER);
      params.setUpdater(Updater.ADAM);
      params.setActivation(Activation.TANH);
      params.setInitSequence("latest trends\n");
      params.setCharsToSample(40);
      params.setSamplesToGen(3);
      params.setEosChar('\n');
      params.setGenEveryNMiniBatch(30);

      CharacterIterator iter = new CharacterIterator(filePath, Charset.forName("UTF-8"),
              params.getMiniBatchSize(), params.getExampleLength());
      params.setCharIter(iter);
      params.setInputLayerSize(iter.inputColumns());
      params.setOutputLayerSize(iter.inputColumns());

      MultiLayerNetwork net = NetworkUtils.trainLSTM(params);

      // using char LSTM lookup to generate new suggestions
      Lookup lookup = new LSTMLookup(iter, net);
      List<String> inputs = new LinkedList<>();
      inputs.addAll(generateInputs("music is my aircraft"));
      inputs.addAll(generateInputs("books about search and deep learning"));
      inputs.addAll(generateInputs("books about search engines and deep learning"));

      for (String input : inputs) {
        List<Lookup.LookupResult> lookupResults = null;
        lookupResults = lookup.lookup(input, false, 3);
        log.info("'" + input + "'");
        for (Lookup.LookupResult result : lookupResults) {
          log.info("--> {} ({})", result.key, result.value);
        }
      }

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

  private static List<String> generateInputs(String input) {
    List<String> inputs = new LinkedList<>();
    for (int i = 1; i < input.length(); i++) {
      inputs.add(input.substring(0, i));
    }
    inputs.add(input);
    return inputs;
  }

}
