package us.csmckay.dl4j.suggest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.suggest.DocumentDictionary;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingInfixSuggester;
import org.apache.lucene.search.suggest.analyzing.FreeTextSuggester;
import org.apache.lucene.search.suggest.jaspell.JaspellLookup;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.utility.BaseUtils;


@SuppressWarnings("deprecation")
public class StdQuerySuggestions {

  private static final Logger log = LoggerFactory.getLogger(StdQuerySuggestions.class);
  private static final String resourcePath = BaseUtils.getResourcePath("queries.txt");
  private static final String workPath = BaseUtils.getHomePath() + "/suggest";

  public static void main(String[] args) {
    try {

      final String queryText = "music is my aircraft";
      File dataFile = new File(workPath + "/queries.txt");
      FileUtils.copyFile(new File(resourcePath), dataFile);
      File dirFile = new File(workPath + "/dir");
      dirFile.mkdirs();
      Directory directory = FSDirectory.open(Paths.get(dirFile.getPath()));

      // dictionary simple approach (query log entries that starts with input)
      Lookup jLookup = new JaspellLookup();
      buildStatic(dataFile, jLookup);
      runLookups(queryText, jLookup);

      // dictionary with text analysis (entries containing input word tokens)
      Lookup asLookup = new AnalyzingInfixSuggester(directory, new StandardAnalyzer(), new StandardAnalyzer(), 0,
              true);
      buildStatic(dataFile, asLookup);
      runLookups(queryText, asLookup);

      // dictionary with ngram-based language model
      Lookup ftLookup = new FreeTextSuggester(new WhitespaceAnalyzer());
      buildStatic(dataFile, ftLookup);
      runLookups(queryText, ftLookup);

      // dynamic content with documents stored in the index
      buildDynamic(BaseUtils.getHomePath() + "/chapter2/index", ftLookup);
      runLookups(queryText, ftLookup);

    } catch (Exception e) {
      log.error("Unexpected error", e);
    }
  }

  private static void buildStatic(File dataFile, Lookup lk) throws IOException {
    log.debug("Loading the static dictionary...");
    long start = System.nanoTime();
    Path path = Paths.get(dataFile.getPath());
    Dictionary dictionary = new PlainTextDictionary(path);
    lk.build(dictionary);
    log.debug("Took {} ms", (System.nanoTime() - start) / (long) 1e6);
    log.debug("Loaded {} entries", lk.getCount());
  }

  private static void buildDynamic(String indexPath, Lookup lk) throws IOException {
    log.debug("Loading the dictionary from the index...");
    long start = System.nanoTime();
    Directory directory = FSDirectory.open(Paths.get(indexPath));
    IndexReader reader = DirectoryReader.open(directory);
    Dictionary dictionary = new DocumentDictionary(reader, "song", "rank");
    lk.build(dictionary);
    log.debug("Took {} ms", (System.nanoTime() - start) / (long) 1e6);
    log.debug("Loaded {} entries", lk.getCount());
  }

  private static void runLookups(String queryText, Lookup lk) throws IOException {
    log.info("Running {}", lk.getClass().getName());
    for (int i = 1; i <= queryText.length(); i++) {
      String sub = queryText.substring(0, i);
      log.info("'{}'", sub);
      List<Lookup.LookupResult> lookupResults = lk.lookup(sub, false, 2);
      for (Lookup.LookupResult result : lookupResults) {
        log.info("--> {} ({})", result.key, result.value);
      }
    }
  }

}
