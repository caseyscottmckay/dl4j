package us.csmckay.dl4j.extension;

import java.io.IOException;
import java.util.Arrays;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.TermStatistics;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.SmallFloat;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import us.csmckay.dl4j.utility.BaseUtils;

/**
 * A {@link Similarity} based on {@link Word2Vec}.
 **/
public class WordEmbeddingsSimilarity extends Similarity {

  private final Word2Vec word2Vec;
  private final String fieldName;
  private final boolean tfidf;

  public WordEmbeddingsSimilarity(Word2Vec word2Vec, String fieldName, boolean tfidf) {
    this.word2Vec = word2Vec;
    this.fieldName = fieldName;
    this.tfidf = tfidf;
  }

  @Override
  public long computeNorm(FieldInvertState state) {
    final int numTerms = state.getLength() - state.getNumOverlap();
    int indexCreatedVersionMajor = state.getIndexCreatedVersionMajor();
    if (indexCreatedVersionMajor >= 7) {
      return SmallFloat.intToByte4(numTerms);
    } else {
      return SmallFloat.floatToByte315((float) (1 / Math.sqrt(numTerms)));
    }
  }

  @Override
  public SimWeight computeWeight(float boost, CollectionStatistics collectionStats, TermStatistics... termStats) {
    return new EmbeddingsSimWeight(boost, collectionStats, termStats);
  }

  @Override
  public SimScorer simScorer(SimWeight weight, LeafReaderContext context) throws IOException {
    return new EmbeddingsSimScorer(weight, context);
  }

  private class EmbeddingsSimScorer extends SimScorer {
    private final EmbeddingsSimWeight weight;
    private final LeafReaderContext context;
    private Terms fieldTerms;
    private LeafReader reader;

    public EmbeddingsSimScorer(SimWeight weight, LeafReaderContext context) {
      this.weight = (EmbeddingsSimWeight) weight;
      this.context = context;
      this.reader = context.reader();
    }

    @Override
    public String toString() {
      return "EmbeddingsSimScorer{" + "weight=" + weight + ", context=" + context + ", fieldTerms=" + fieldTerms
              + ", reader=" + reader + '}';
    }

    @Override
    public float score(int doc, float freq) throws IOException {
      INDArray queryVector = getQueryVector();
      INDArray documentVector = BaseUtils.toDenseMeanVector(word2Vec, reader.getTermVector(doc, fieldName),
              reader.numDocs(), true);
      return (float) Transforms.cosineSim(queryVector, documentVector);
    }

    private INDArray getQueryVector() throws IOException {
      INDArray queryVector = Nd4j.zeros(word2Vec.getLayerSize());
      String[] queryTerms = new String[weight.termStats.length];
      int i = 0;
      for (TermStatistics termStats : weight.termStats) {
        queryTerms[i] = termStats.term().utf8ToString();
        i++;
      }

      if (fieldTerms == null) {
        fieldTerms = MultiFields.getTerms(reader, fieldName);
      }

      for (String queryTerm : queryTerms) {
        TermsEnum iter = fieldTerms.iterator();
        BytesRef term;
        while ((term = iter.next()) != null) {
          TermsEnum.SeekStatus seekStatus = iter.seekCeil(term);
          if (seekStatus.equals(TermsEnum.SeekStatus.END)) {
            iter = fieldTerms.iterator();
          }
          if (seekStatus.equals(TermsEnum.SeekStatus.FOUND)) {
            String string = term.utf8ToString();
            if (string.equals(queryTerm)) {
              INDArray wordVector = word2Vec.getLookupTable().vector(queryTerm);
              if (wordVector != null) {
                double smoothFactor;
                if (tfidf) {
                  smoothFactor = BaseUtils.tfIdf(reader.numDocs(), iter.totalTermFreq(),
                          iter.docFreq());
                } else {
                  smoothFactor = queryTerms.length;
                }
                queryVector.addi(wordVector.div(smoothFactor));
              }
              break;
            }
          }
        }
      }
      return queryVector;
    }

    @Override
    public float computeSlopFactor(int distance) {
      return 1;
    }

    @Override
    public float computePayloadFactor(int doc, int start, int end, BytesRef payload) {
      return 1;
    }
  }

  private class EmbeddingsSimWeight extends SimWeight {
    private final float boost;
    private final CollectionStatistics collectionStats;
    private final TermStatistics[] termStats;

    public EmbeddingsSimWeight(float boost, CollectionStatistics collectionStats, TermStatistics[] termStats) {
      this.boost = boost;
      this.collectionStats = collectionStats;
      this.termStats = termStats;
    }

    @Override
    public String toString() {
      return "EmbeddingsSimWeight{" + "boost=" + boost + ", collectionStats=" + collectionStats + ", termStats="
              + Arrays.toString(termStats) + '}';
    }
  }

}
