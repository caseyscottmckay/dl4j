package us.csmckay.dl4j.extension;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.search.suggest.InputIterator;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.store.DataInput;
import org.apache.lucene.store.DataOutput;
import org.apache.lucene.util.BytesRef;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import us.csmckay.dl4j.utility.NetworkUtils;

/**
 * LSTM based {@link Lookup} implementation.
 */
public class LSTMLookup extends Lookup {

  private CharacterIterator charIter;
  private MultiLayerNetwork net;
  private NetworkUtils.NetParams tp;

  public LSTMLookup(CharacterIterator charIter, MultiLayerNetwork net) {
    this.charIter = charIter;
    this.net = net;
  }

  public LSTMLookup(NetworkUtils.NetParams tp) {
    this.tp = tp;
  }

  @Override
  public long getCount() throws IOException {
    return -1L;
  }

  @Override
  public void build(InputIterator inputIterator) throws IOException {
    Path tempFile = Files.createTempFile("chars", ".txt");
    FileOutputStream outputStream = new FileOutputStream(tempFile.toFile());
    for (BytesRef surfaceForm; (surfaceForm = inputIterator.next()) != null; )
      outputStream.write(surfaceForm.bytes);
    outputStream.flush();
    outputStream.close();
    this.charIter = new CharacterIterator(tempFile.toAbsolutePath().toString(), Charset.defaultCharset(),
            tp.getMiniBatchSize(), tp.getExampleLength());
    tp.setCharIter(charIter);
    tp.setInputLayerSize(charIter.inputColumns());
    tp.setOutputLayerSize(charIter.inputColumns());
    this.net = NetworkUtils.trainLSTM(tp);
    FileUtils.forceDeleteOnExit(tempFile.toFile());
  }

  @Override
  public List<LookupResult> lookup(CharSequence key, Set<BytesRef> contexts, boolean onlyMorePopular, int num)
          throws IOException {
    tp.setInitSequence(key.toString());
    tp.setSamplesToGen(num);
    List<NetworkUtils.Sample> genSamples = NetworkUtils.sampleFromNetwork(charIter, net, tp);
    List<LookupResult> results = new LinkedList<>();
    for (NetworkUtils.Sample s : genSamples)
      results.add(new LookupResult(s.getValue(), (int) s.getProb()));
    return results;
  }

  @Override
  public boolean store(DataOutput output) throws IOException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public boolean load(DataInput input) throws IOException {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public long ramBytesUsed() {
    throw new UnsupportedOperationException("Not implemented");
  }

}
