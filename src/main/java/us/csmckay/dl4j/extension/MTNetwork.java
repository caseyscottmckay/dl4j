package us.csmckay.dl4j.extension;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.xml.stream.XMLStreamException;

import com.google.common.base.Joiner;
import org.apache.commons.lang3.ArrayUtils;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.graph.rnn.DuplicateToTimeSeriesVertex;
import org.deeplearning4j.nn.conf.graph.rnn.LastTimeStepVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.EmbeddingLayer;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.graph.vertex.GraphVertex;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.utility.ParallelCorpusProcessor;

public class MTNetwork implements TranslatorTool {

  private static final Logger log = LoggerFactory.getLogger(MTNetwork.class);
  private static final Random rnd = new Random(new Date().getTime());

  private static final int TBPTT_SIZE = 25;
  private static final int EMBEDDING_WIDTH = 100;
  private static final int HIDDEN_LAYER_WIDTH = 200;
  public static final int ROW_SIZE = 50;
  private static final int MINIBATCH_SIZE = 4;
  private static final String MODEL_FILENAME = "mt-lstm-train.zip";
  private static final long TEST_EACH_MS = TimeUnit.MINUTES.toMillis(2);
  private static final long SAVE_EACH_MS = TimeUnit.MINUTES.toMillis(10);
  private static final int EPOCHS = 10;

  private final ComputationGraph net;
  private ParallelCorpusProcessor corpusProcessor;

  public MTNetwork(String tmxFile, String sourceCode, String targetCode, IterationListener... listeners)
          throws IOException, XMLStreamException {
    log.info("Processing parallel corpus...");
    corpusProcessor = new ParallelCorpusProcessor(tmxFile, sourceCode, targetCode, true);
    corpusProcessor.process();

    Map<String, Double> dictionary = corpusProcessor.getDict();

    File networkFile = new File(toTempPath(MODEL_FILENAME));
    if (networkFile.exists()) {
      log.info("Loading the existing network...");
      net = ModelSerializer.restoreComputationGraph(networkFile);
    } else {
      log.info("Creating a new network...");
      net = createComputationGraph(dictionary.size());
    }

    net.init();
    net.setListeners(listeners);
    train();
  }

  public MTNetwork(File modelFile, String tmxFile, String sourceCode, String targetCode,
                   IterationListener... listeners) throws IOException, XMLStreamException {
    corpusProcessor = new ParallelCorpusProcessor(tmxFile, sourceCode, targetCode, true);
    corpusProcessor.process();
    net = ModelSerializer.restoreComputationGraph(modelFile);
    net.init();
  }

  private ComputationGraph createComputationGraph(int dictionarySize) {
    final NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder().updater(new RmsProp())
            .weightInit(WeightInit.XAVIER).gradientNormalization(GradientNormalization.RenormalizeL2PerLayer);

    final ComputationGraphConfiguration.GraphBuilder graphBuilder = builder.graphBuilder().pretrain(false)
            .backprop(true).backpropType(BackpropType.Standard).tBPTTBackwardLength(TBPTT_SIZE)
            .tBPTTForwardLength(TBPTT_SIZE).addInputs("inputLine", "decoderInput")
            .setInputTypes(InputType.recurrent(dictionarySize), InputType.recurrent(dictionarySize))
            .addLayer("embeddingEncoder",
                    new EmbeddingLayer.Builder().nIn(dictionarySize).nOut(EMBEDDING_WIDTH).build(), "inputLine")
            .addLayer("encoder",
                    new GravesLSTM.Builder().nIn(EMBEDDING_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "embeddingEncoder")
            .addLayer("encoder2",
                    new GravesLSTM.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "encoder")
            .addLayer("encoder3",
                    new GravesLSTM.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "encoder2")
            .addVertex("thoughtVector", new LastTimeStepVertex("inputLine"), "encoder3")
            .addVertex("dup", new DuplicateToTimeSeriesVertex("decoderInput"), "thoughtVector")
            .addVertex("merge", new MergeVertex(), "decoderInput", "dup")
            .addLayer("decoder",
                    new GravesLSTM.Builder().nIn(dictionarySize + HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "merge")
            .addLayer("decoder2",
                    new GravesLSTM.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "decoder")
            .addLayer("decoder3",
                    new GravesLSTM.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(HIDDEN_LAYER_WIDTH)
                            .activation(Activation.TANH).build(),
                    "decoder2")
            .addLayer("output",
                    new RnnOutputLayer.Builder().nIn(HIDDEN_LAYER_WIDTH).nOut(dictionarySize)
                            .activation(Activation.SOFTMAX).lossFunction(LossFunctions.LossFunction.MCXENT).build(),
                    "decoder3")
            .setOutputs("output");

    return new ComputationGraph(graphBuilder.build());
  }

  private void train() throws IOException {
    File networkFile = new File(toTempPath(MODEL_FILENAME));
    long lastTestTime = System.nanoTime();
    long lastSaveTime = System.nanoTime();
    ParallelCorpusIterator parallelCorpusIterator = new ParallelCorpusIterator(corpusProcessor, MINIBATCH_SIZE,
            ROW_SIZE);
    for (int epoch = 1; epoch < EPOCHS; ++epoch) {
      log.info("Starting epoch {}", epoch);
      int lastPerc = 0;
      while (parallelCorpusIterator.hasNext()) {
        MultiDataSet multiDataSet = parallelCorpusIterator.next();
        net.fit(multiDataSet);

        int newPerc = (parallelCorpusIterator.batch() * 100 / parallelCorpusIterator.totalBatches());
        if (newPerc != lastPerc) {
          log.info("Epoch complete: {} %", newPerc);
          lastPerc = newPerc;
        }
        if (((System.nanoTime() - lastTestTime) / (long) 1e6) > TEST_EACH_MS) {
          test();
          lastTestTime = System.nanoTime();
        }
        if (((System.nanoTime() - lastSaveTime) / (long) 1e6) > SAVE_EACH_MS) {
          saveModel(networkFile);
          lastSaveTime = System.nanoTime();
        }
      }
    }
  }

  private void test() {
    log.debug("New test...");
    int selected = rnd.nextInt(corpusProcessor.getCorpus().size() - 1);
    if (selected % 2 == 1) {
      selected++;
    }
    List<Double> rowIn = new ArrayList<>(corpusProcessor.getCorpus().get(selected));
    StringBuffer sb = new StringBuffer();
    for (Double idx : rowIn) {
      sb.append(corpusProcessor.getRevDict().get(idx) + " ");
    }
    log.debug("Input: {}", sb.toString());
    Collection<String> out = output(rowIn, true, 0);
    sb = new StringBuffer();
    for (String o : out) {
      sb.append(o);
    }
    log.debug("Output: {}", sb.toString());

  }

  public Collection<String> output(String query, double score) {
    Collection<String> tokens = new LinkedList<>();
    corpusProcessor.tokenizeLine(query, tokens);
    List<Double> doubles = corpusProcessor.wordsToIndexes(tokens);
    return output(doubles, false, score);
  }

  private Collection<String> output(List<Double> rowIn, boolean printUnknowns, double score) {
    Collection<String> result = new LinkedList<>();
    net.rnnClearPreviousState();
    Collections.reverse(rowIn);
    Double[] array = rowIn.toArray(new Double[0]);
    INDArray in = Nd4j.create(ArrayUtils.toPrimitive(array), new int[]{1, 1, rowIn.size()});
    int size = corpusProcessor.getDict().size();
    double[] decodeArr = new double[size];
    decodeArr[2] = 1;
    INDArray decode = Nd4j.create(decodeArr, new int[]{1, size, 1});
    net.feedForward(new INDArray[]{in, decode}, false);
    org.deeplearning4j.nn.layers.recurrent.GravesLSTM decoder = (org.deeplearning4j.nn.layers.recurrent.GravesLSTM) net
            .getLayer("decoder");
    Layer output = net.getLayer("output");
    GraphVertex mergeVertex = net.getVertex("merge");
    INDArray thoughtVector = mergeVertex.getInputs()[1];
    for (int row = 0; row < rowIn.size(); ++row) {
      mergeVertex.setInputs(decode, thoughtVector);
      INDArray merged = mergeVertex.doForward(false);
      INDArray activateDec = decoder.rnnTimeStep(merged);
      INDArray out = output.activate(activateDec, false);
      double d = rnd.nextDouble();
      double sum = 0.0;
      int idx = 0;
      for (int s = 0; s < out.size(1); s++) {
        sum += out.getDouble(0, s, 0);
        if (d <= sum) {
          idx = s;
          if (printUnknowns || s != 0) {
            String s1 = corpusProcessor.getRevDict().get((double) s) + " ";
            result.add(s1);
          }
          break;
        }
      }
      if (idx == 1) {
        break;
      }
      double[] newDecodeArr = new double[size];
      newDecodeArr[idx] = 1;
      decode = Nd4j.create(newDecodeArr, new int[]{1, size, 1});
    }
    return result;
  }

  private void saveModel(File networkFile) throws IOException {
    log.info("Saving the model...");
    File backup = new File(toTempPath(MODEL_FILENAME));
    if (networkFile.exists()) {
      if (backup.exists()) {
        backup.delete();
      }
      networkFile.renameTo(backup);
    }
    ModelSerializer.writeModel(net, networkFile, true);
    log.info("Model saved to {}", networkFile.getAbsolutePath());
  }

  private String toTempPath(String path) {
    return System.getProperty("java.io.tmpdir") + "/" + path;
  }

  @Override
  public Collection<Translation> translate(String text) {
    double score = 0d;
    String string = Joiner.on(' ').join(output(text, score));
    Translation translation = new Translation(string, score);
    return Collections.singletonList(translation);
  }

}
