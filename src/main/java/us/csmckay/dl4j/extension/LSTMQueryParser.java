package us.csmckay.dl4j.extension;

import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.csmckay.dl4j.utility.NetworkUtils;
import us.csmckay.dl4j.utility.NetworkUtils.NetParams;
import us.csmckay.dl4j.utility.NetworkUtils.Sample;

/**
 * LSTM based {@link QueryParser} implementation.
 **/
public class LSTMQueryParser extends QueryParser {

  private static final Logger log = LoggerFactory.getLogger(LSTMQueryParser.class);
  private final MultiLayerNetwork net;
  private final CharacterIterator iter;

  public LSTMQueryParser(String field, Analyzer analyzer, MultiLayerNetwork net, CharacterIterator iter) {
    super(field, analyzer);
    this.net = net;
    this.iter = iter;
  }

  @Override
  public Query parse(String query) throws ParseException {
    BooleanQuery.Builder builder = new BooleanQuery.Builder();
    builder.add(new BooleanClause(super.parse(query), BooleanClause.Occur.MUST));

    // we add a carriage return to the input query to hint
    // the net to try to generate a completely new query
    NetParams np = new NetParams();
    np.setInitSequence(query + "\n");
    np.setCharsToSample(40);
    np.setSamplesToGen(3);
    np.setEosChar('\n');

    List<Sample> genSamples = NetworkUtils.sampleFromNetwork(iter, net, np);
    for (Sample s : genSamples) {
      String value = s.getValue();
      if (value.contains("\n"))
        value = value.substring(value.indexOf("\n"));
      log.debug(query + " -> " + value);
      builder.add(new BooleanClause(super.parse(value), BooleanClause.Occur.SHOULD));
    }

    return builder.build();
  }

}
