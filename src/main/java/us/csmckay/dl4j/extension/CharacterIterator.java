package us.csmckay.dl4j.extension;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom {@link DataSetIterator} for text files. Given a text file and a few
 * options, generate feature vectors and labels for training, where we want to
 * predict the next character in the sequence. This is done by randomly choosing
 * a position in the text file, at offsets of 0, exampleLength, 2*exampleLength,
 * etc to start each sequence. Then we convert each character to an index, i.e.,
 * a one-hot vector. Then the character 'a' becomes [1,0,0,0,...], 'b' becomes
 * [0,1,0,0,...], etc. Feature vectors and labels are both one-hot vectors of
 * same length.
 */
public class CharacterIterator implements DataSetIterator {

  private static final long serialVersionUID = 1L;
  private static final Logger log = LoggerFactory.getLogger(CharacterIterator.class);
  private static Random rng = new Random(12345);

  private char[] validChars;
  private LinkedList<Integer> sampleOffsets;
  private Map<Character, Integer> charToIndexMap;
  private int sampleLength; // number of characters of each sample
  private int miniBatchSize; // number of examples in each mini batch
  private char[] fileChars;

  /**
   * @param txtPath       Path to the text file to use for generating examples.
   * @param txtEncoding   Encoding of the text file.
   * @param batchSize     Number of examples per mini-batch.
   * @param exampleLength Number of characters in each input/output vector.
   * @throws IOException If text file cannot be loaded.
   */
  public CharacterIterator(String txtPath, Charset txtEncoding, int batchSize, int exampleLength) throws IOException {
    File textFile = new File(txtPath);
    if (!textFile.exists())
      throw new IOException("Could not read the file" + txtPath);
    if (batchSize <= 0)
      throw new IllegalArgumentException("Invalid mini-batch size (must be > 0)");

    this.sampleOffsets = new LinkedList<>();
    this.charToIndexMap = new HashMap<>();
    this.validChars = getMinimalCharacterSet();
    this.sampleLength = exampleLength;
    this.miniBatchSize = batchSize;

    for (int i = 0; i < validChars.length; i++)
      charToIndexMap.put(validChars[i], i);

    int maxSize = initializeFileChars(txtEncoding, textFile);

    if (exampleLength >= fileChars.length)
      throw new IllegalArgumentException("exampleLength=" + exampleLength
              + " cannot exceed the number of valid chars in file (" + fileChars.length + ")");

    log.debug("Loaded file: " + fileChars.length + " valid chars of " + maxSize + " total chars ("
            + (maxSize - fileChars.length) + " removed)");

    initializeOffsets();
  }

  public static char[] getMinimalCharacterSet() {
    List<Character> validChars = new LinkedList<>();
    for (char c = 'a'; c <= 'z'; c++)
      validChars.add(c);
    for (char c = 'A'; c <= 'Z'; c++)
      validChars.add(c);
    for (char c = '0'; c <= '9'; c++)
      validChars.add(c);
    char[] temp = {'!', '&', '(', ')', '?', '-', '\'', '"', ',', '.', ':', ';', ' ', '\n', '\t'};
    for (char c : temp)
      validChars.add(c);
    char[] out = new char[validChars.size()];
    int i = 0;
    for (Character c : validChars)
      out[i++] = c;
    return out;
  }

  public static char[] getDefaultCharacterSet() {
    List<Character> validChars = new LinkedList<>();
    for (char c : getMinimalCharacterSet())
      validChars.add(c);
    char[] additionalChars = {'@', '#', '$', '%', '^', '*', '{', '}', '[', ']', '/', '+', '_', '\\', '|', '<',
            '>'};
    for (char c : additionalChars)
      validChars.add(c);
    char[] out = new char[validChars.size()];
    int i = 0;
    for (Character c : validChars)
      out[i++] = c;
    return out;
  }

  public int initializeFileChars(Charset txtEncoding, File txtFile) throws IOException {
    boolean isNewLineValid = charToIndexMap.containsKey('\n');
    List<String> lines = Files.readAllLines(txtFile.toPath(), txtEncoding);
    int maxSize = lines.size(); // total newline chars
    for (String s : lines)
      maxSize += s.length();
    char[] chars = new char[maxSize];
    int index = 0;
    for (String s : lines) {
      char[] line = s.toCharArray();
      for (char c : line) {
        if (!charToIndexMap.containsKey(c))
          continue; // filtering not valid chars
        chars[index++] = c;
      }
      if (isNewLineValid)
        chars[index++] = '\n';
    }
    if (index == chars.length)
      fileChars = chars;
    else
      fileChars = Arrays.copyOfRange(chars, 0, index);
    return maxSize;
  }

  private void initializeOffsets() {
    int minibatchesPerEpoch = (fileChars.length - 1) / sampleLength - 2;
    for (int i = 0; i < minibatchesPerEpoch; i++)
      sampleOffsets.add(i * sampleLength);
    Collections.shuffle(sampleOffsets, rng);
  }

  public char getCharacter(int idx) {
    return validChars[idx];
  }

  public int getCharacterIndex(char c) {
    return charToIndexMap.get(c);
  }

  public char getRandomCharacter() {
    return validChars[(int) (rng.nextDouble() * validChars.length)];
  }

  @Override
  public boolean hasNext() {
    return sampleOffsets.size() > 0;
  }

  @Override
  public DataSet next() {
    return next(miniBatchSize);
  }

  @Override
  public DataSet next(int num) {
    if (sampleOffsets.size() == 0)
      throw new NoSuchElementException();

    // dimension 0 = number of samples in a mini-batch
    // dimension 1 = length of each hot-vector
    // dimension 2 = sample length (number of time steps)
    int currMinibatchSize = Math.min(num, sampleOffsets.size());
    INDArray features = Nd4j.create(new int[]{currMinibatchSize, validChars.length, sampleLength}, 'f');
    INDArray labels = Nd4j.create(new int[]{currMinibatchSize, validChars.length, sampleLength}, 'f');

    for (int i = 0; i < currMinibatchSize; i++) {
      int startIdx = sampleOffsets.removeFirst();
      int endIdx = startIdx + sampleLength;
      int currCharIdx = charToIndexMap.get(fileChars[startIdx]); // current char
      int c = 0;
      for (int j = startIdx + 1; j < endIdx; j++, c++) {
        int nextCharIdx = charToIndexMap.get(fileChars[j]); // next char to predict
        features.putScalar(new int[]{i, currCharIdx, c}, 1.0);
        labels.putScalar(new int[]{i, nextCharIdx, c}, 1.0);
        currCharIdx = nextCharIdx;
      }
    }

    return new DataSet(features, labels);
  }

  @Override
  public int totalExamples() {
    return (fileChars.length - 1) / miniBatchSize - 2;
  }

  @Override
  public int inputColumns() {
    return validChars.length;
  }

  @Override
  public int totalOutcomes() {
    return validChars.length;
  }

  @Override
  public void reset() {
    sampleOffsets.clear();
    initializeOffsets();
  }

  @Override
  public boolean resetSupported() {
    return true;
  }

  @Override
  public boolean asyncSupported() {
    return true;
  }

  @Override
  public int batch() {
    return miniBatchSize;
  }

  @Override
  public int cursor() {
    return totalExamples() - sampleOffsets.size();
  }

  @Override
  public int numExamples() {
    return totalExamples();
  }

  @Override
  public void setPreProcessor(DataSetPreProcessor preProcessor) {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public DataSetPreProcessor getPreProcessor() {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public List<String> getLabels() {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException("Not implemented");
  }

}