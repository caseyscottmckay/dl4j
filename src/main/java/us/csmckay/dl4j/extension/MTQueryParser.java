package us.csmckay.dl4j.extension;

import java.util.Collection;
import java.util.Map;

import opennlp.tools.langdetect.Language;
import opennlp.tools.langdetect.LanguageDetector;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link QueryParser} that takes the original user entered query, recognizes
 * the language via openNLP's {@link LanguageDetector}, picks up the
 * {@link TranslatorTool}s that can translate from the given language, perform
 * decoding (translation), parses the translated queries and then adds them as
 * optional queries in a {@link BooleanQuery}.
 **/
public class MTQueryParser extends QueryParser {

  private static final Logger log = LoggerFactory.getLogger(MTQueryParser.class);

  private final LanguageDetector languageDetector;
  private final Map<String, Collection<TranslatorTool>> decoderMappings;

  public MTQueryParser(String f, Analyzer a, LanguageDetector languageDetector,
                       Map<String, Collection<TranslatorTool>> decoderMappings) {
    super(f, a);
    this.languageDetector = languageDetector;
    this.decoderMappings = decoderMappings;
  }

  @Override
  public Query parse(String query) throws ParseException {
    BooleanQuery.Builder builder = new BooleanQuery.Builder();
    builder.add(new BooleanClause(super.parse(query), BooleanClause.Occur.SHOULD));

    Language language = languageDetector.predictLanguage(query);
    String languageKey = language.getLang();
    if (languageKey == null) {
      languageKey = "eng";
    }
    log.info("Detected language {} for query '{}'", languageKey, query);

    Collection<TranslatorTool> decoders = decoderMappings.get(languageKey);
    if (decoders == null) {
      decoders = decoderMappings.get("eng"); // use default eng->xyz decoders
      if (decoders == null) {
        decoders = decoderMappings.get("ita");
      }
    }

    for (TranslatorTool d : decoders) {
      Collection<TranslatorTool.Translation> translations = d.translate(query);
      log.info("Found {} translations", translations.size());

      // create and bind translated queries
      for (TranslatorTool.Translation translation : translations) {
        log.info("{}", translation);
        String text = translation.getText();
        builder.add(new BooleanClause(super.parse(text), BooleanClause.Occur.SHOULD));
      }
    }

    return builder.build();
  }

}