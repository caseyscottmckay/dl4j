package us.csmckay.dl4j.extension;

import java.util.Collection;

/***/
public interface TranslatorTool {

  public Collection<Translation> translate(String query);

  public class Translation {
    private String text;
    private double score;

    public Translation(String text, double score) {
      this.text = text;
      this.score = score;
    }

    public String getText() {
      return text;
    }

    public double getScore() {
      return score;
    }
  }

}
